// Wait for an element on the DOM
function elementReady(selector) {
    return new Promise(resolve => {
        // First check if it's already on the page
        const element = document.querySelector(selector)
        if (element) {
            resolve(element)
        }
        // Else set a mutation observer on the document
        new MutationObserver(function() {
            const element = document.querySelector(selector)
            if (element) {
                resolve(element)
                // Stop observations because element was finded
                this.disconnect()
            }
        }).observe(document.documentElement, { childList: true, subtree: true })
    })
}

// Hide the twitch chat
async function hideChat() {
    // Wait for the welcome message mean chat is ready
    await elementReady('[data-a-target="chat-welcome-message"]')
    // Wait for settings button then click it to open settings dialog
    const eSettingsButton = await elementReady('[data-a-target="chat-settings"]')
    eSettingsButton.click()
    // Wait for hide chat button on dialog then click it
    const eHideButton = await elementReady('[data-a-target="hide-chat-button"]')
    eHideButton.click()
}

// Start script with the storage result
function start(twitchHideChat) {
    // Hide chat if boolean is true
    if (twitchHideChat) {
        hideChat()
    }
}

// Start execution by getting the stored boolean
chrome.storage.sync.get('twitchHideChat', result => start(result.twitchHideChat))

/*
// Storage listener to detect when boolean update
chrome.storage.onChanged.addListener((changes, namespace) => {
    // If the change is on the attribute twitchHideChat
    if (changes.twitchHideChat && namespace === 'sync') {
        start(changes.twitchHideChat.newValue)
    }
})
*/