// When click on the icon
chrome.browserAction.onClicked.addListener(() => {
    // Get stored boolean value
    chrome.storage.sync.get('twitchHideChat', result => {
        // Set it to opposite
        chrome.storage.sync.set({ 'twitchHideChat': !result.twitchHideChat })
        // Change icon ans title (true is the red, mean chat hidded)
        const state = (!result.twitchHideChat) ? 'hidded' : 'visible'
        chrome.browserAction.setIcon({ path: '/icon_' + state + '.png' })
        chrome.browserAction.setTitle({ title: 'Twitch chat ' + state })
    })
})